import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // url: string = 'http://192.168.0.7:8000/app';
  url: string = 'http://localhost:8000/app'
  constructor(private http: HttpClient) { }
  get(path): Observable<any> {
    return this.http.get(this.url + '/' + path);
  }
  post(path, body): Observable<any> {
    return this.http.post(this.url + '/' + path, body);
  }
}
