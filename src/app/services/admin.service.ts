import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private api: ApiService) { }
  createuser(body) {
    return this.api.post('createuser', body);
  }
}
