import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from './services/admin.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild("video")
  public video: ElementRef;
  @ViewChild("canvas")
  public canvas: ElementRef;

  image: any;
  message: string = ''
  register: FormGroup;
  name_photo: string = '';
  base64: string;
  loader: boolean = false;
  constructor(private service: AdminService) {
    // this.capture = [];
    this.register = this.formreg();
  }
  ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
        // console.log(stream)
        this.video.nativeElement.srcObject = stream;
        this.video.nativeElement.play();
      });
    }
  }

  capture() {
    var context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 640, 480);
    this.image = this.canvas.nativeElement.toDataURL("image/png");
    var s = this.image;
    // console.log(s)
    this.base64 = s.replace('data:image/png;base64,', '');
  }

  formreg() {
    return new FormGroup({
      username: new FormControl('', [Validators.required]),
      last_name: new FormControl(''),
      email: new FormControl('', [Validators.required]),
      phone: new FormControl(''),
    })
  }
  updloadfile(event) {
    this.name_photo = event.target.files[0].name
    // base 64
    var self = this;
    var f = event.target.files[0];
    var reader = new FileReader();
    reader.onload = (function (theFile) {
      return function (e) {
        var binaryData = e.target.result;
        // self.loadImage(window.btoa(binaryData))
        self.base64 = window.btoa(binaryData)
      }
    })(f);
    reader.readAsBinaryString(f);
  }
  saveUser() {
    var name_img: string = this.register.controls['username'].value
    const send = {
      username: this.register.controls['username'].value,
      last_name: this.register.controls['last_name'].value,
      email: this.register.controls['email'].value,
      phone: this.register.controls['phone'].value,
      image: this.base64,
      name_image: name_img.replace(/ /g, "") + '.png'
    }
    // console.log(send)
    if (this.register.valid && this.base64) {
      console.log(send)
      this.loader = true;
      this.service.createuser(send).subscribe((resp) => {
        console.log(resp)
        this.loader = false;
        this.message = 'Usuario registrado correctamente'
      }, (err) => {
        this.loader = false;
        console.log(err)
        this.message = 'Ocurrio un error, intentalo mas tarde'
      });
    }
  }
  example() {

  }

}
